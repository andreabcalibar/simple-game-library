#include "wx_pch.h"
#include "simple_game_libraryApp.h"

//(*AppHeaders
#include "simple_game_libraryMain.h"
#include <wx/image.h>
//*)

IMPLEMENT_APP(simple_game_libraryApp);

bool simple_game_libraryApp::OnInit()
{
    //(*AppInitialize
    bool wxsOK = true;
    wxInitAllImageHandlers();
    if ( wxsOK )
    {
    	simple_game_libraryFrame* Frame = new simple_game_libraryFrame(0);
    	Frame->Show();
    	SetTopWindow(Frame);
    }
    //*)
    return wxsOK;

}
