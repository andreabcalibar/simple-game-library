#ifndef SIMPLE_GAME_LIBRARYAPP_H
#define SIMPLE_GAME_LIBRARYAPP_H

#include <wx/app.h>

class simple_game_libraryApp : public wxApp
{
    public:
        virtual bool OnInit();
};

#endif // SIMPLE_GAME_LIBRARYAPP_H
