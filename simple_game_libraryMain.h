#ifndef SIMPLE_GAME_LIBRARYMAIN_H
#define SIMPLE_GAME_LIBRARYMAIN_H

//(*Headers(simple_game_libraryFrame)
#include <wx/frame.h>
#include <wx/menu.h>
#include <wx/statusbr.h>
//*)

class simple_game_libraryFrame: public wxFrame
{
    public:

        simple_game_libraryFrame(wxWindow* parent,wxWindowID id = -1);
        virtual ~simple_game_libraryFrame();

    private:

        //(*Handlers(simple_game_libraryFrame)
        void OnQuit(wxCommandEvent& event);
        void OnAbout(wxCommandEvent& event);
        //*)

        //(*Identifiers(simple_game_libraryFrame)
        static const long idMenuQuit;
        static const long idMenuAbout;
        static const long ID_STATUSBAR1;
        //*)

        //(*Declarations(simple_game_libraryFrame)
        wxStatusBar* StatusBar1;
        //*)

        DECLARE_EVENT_TABLE()
};

#endif // SIMPLE_GAME_LIBRARYMAIN_H
