# Simple Game Library

Simple Game Library is an open-source tool to organize and manage a gaming collection, adding games to a list and marking them as completed, currently playing, played, not played, on hold or dropped.

## Features

- Game List Management: Storing and managing games.
- Flexible Game Status Tracking: Categorize games based on their current status, be it completed, currently playing, played, unplayed, dropped, or on-hold.
- Detailed Game Information: Add game information including its title, genre, description, release date, play status and play time.
- Searching and Filtering: Find specific games within the library using filters like title, genre, or any other criteria.
- Sorting: Sort games by genre, release date, or title.
- Data Import and Export: Import or export the game library data indicating what data to export.

## Tools used

- Code::Blocks 20.03
- wxWidgets 3.1.4
- wxSmith

## Compiling from source

### Paths

- Linux: It uses the default compiled-from-source wxWidgets path.
- Windows: Add a global variable in Code::Blocks named "wx" with the following:
  - base: wxWidgets installation path.
  - include: wxWidgets "include" sub-folder path.
  - lib: wxWidgets "lib" sub-folder path.

### Build Options

- Linux: Use Debug or Release build options.
- Windows: Use DebugWin or ReleaseWin build options.

## License

Game Library is distributed under the LGPL 2.1 license, refer to the LICENSE file available in the project repository.
